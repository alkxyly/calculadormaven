package Simulacao;

import java.util.List;

import AlimentoNutrientes.AlimentoNutriente;
import Origem.Origem;

/**
 * 
 * @author Rodrigo
 *
 */

public class SuinoSimulacaoRN {

	/**
	 * Calcula a energia digestivel suino
	 * 
	 * fórmula EMaves = 4,31(PBd)+9,29(GD)+4,14(ENNd)
	 * 
	 * @param listaNutrientes
	 *            e orig lista com os nutrientes que entram na simulação e a
	 *            origem.
	 * @return valor da energia digestivel
	 */

	public double calcularSuinosEnergiaDigestivel(List<AlimentoNutriente> listaNutrientes, Origem orig) {

		Double EdSuino = 0.0;

		if (orig.equals("Vegetal")) {
			EdSuino = suinoEnergiaDigestivelVegetalLacteos(listaNutrientes);
		} else {
			EdSuino = suinoEnergiaDigestivelAnimalGorduras(listaNutrientes);
		}

		return EdSuino;
	}

	/**
	 * Calcula a energia metabolizavel suino
	 * 
	 * fórmula EMaves = 4,31(PBd)+9,29(GD)+4,14(ENNd)
	 * 
	 * @param listaNutrientes
	 *            e orig lista com os nutrientes que entram na simulação e a
	 *            origem.
	 * @return valor da energia digestivel
	 */

	public double calcularSuinosEnergiaMetabolizavel(List<AlimentoNutriente> listaNutrientes, Origem orig) {

		Double EmSuino = 0.0;

		if (orig.equals("Vegetal")) {
			EmSuino = suinoEnergiaDigestivelVegetalLacteos(listaNutrientes);
		}
		if (orig.equals("Animal")) {
			EmSuino = suinoEnergiaMetabolizavelAnimal(listaNutrientes);
		}
		if (orig.equals("Gordura")) {
			EmSuino = suinoEnergiaMetabolizavelCarboidratosGorduras(listaNutrientes);
		}
		return EmSuino;
	}

	/**
	 * Calcula a energia liquida suino
	 * 
	 * fórmula EMaves = 4,31(PBd)+9,29(GD)+4,14(ENNd)
	 * 
	 * @param listaNutrientes
	 * 
	 * @return valor da energia digestivel
	 */
	public double calcularSuinosEnergiaLiquida(List<AlimentoNutriente> listaNutrientes) {

		return suinoEnergiaLiquida(listaNutrientes);
	}

	/**
	 * Calcula a energia disgetivel dos alimentos de origem vegetal e lacteos.
	 * 
	 * fórmula EMaves = 4,31(PBd)+9,29(GD)+4,14(ENNd)
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */
	public Double suinoEnergiaDigestivelVegetalLacteos(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double g = 0.0; // gordura bruta
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		Double MOd = 0.0;
		Double EDsuino = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {
			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta " + PB);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				g = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura bruta " + g);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta digestivel " + PBd);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura digestivel " + GD);
			}

		}

		EDsuino = (5.65 * (PB * (PBd / 100))) + (9.45 * (g * (GD / 100))) + (4.14 * (MOd - PBd - GD));

		return EDsuino * 10;
	}

	/**
	 * Calcula a energia disgetivel dos alimentos de origem animal e gordura.
	 * 
	 * fórmula EMaves = 4,31(PBd)+9,29(GD)+4,14(ENNd)
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */
	public Double suinoEnergiaDigestivelAnimalGorduras(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double g = 0.0; // gordura bruta
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		Double EDsuino = 0.0; // Energia digestivel suinos

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {
			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta " + PB);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				g = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura bruta " + g);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta digestivel " + PBd);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura digestivel " + GD);
			}
		}

		EDsuino = (5.65 * (PB * (PBd / 100))) + (9.45 * (g * (GD / 100)));

		return EDsuino * 10;
	}

	/**
	 * Calcula a energia metabolizavel dos alimentos de origem vegetal e
	 * lacteos.
	 * 
	 * fórmula EMaves = 4,31(PBd)+9,29(GD)+4,14(ENNd)
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */
	public Double suinoEnergiaMetabolizavelVegetalLacteos(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double g = 0.0; // gordura bruta
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		Double MOd = 0.0;
		Double EMsuino = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {
			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta " + PB);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				g = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura bruta " + g);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta digestivel " + PBd);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura digestivel " + GD);
			}

		}

		EMsuino = (4.952 * (PB * (PBd / 100))) + (9.45 * (g * (GD / 100))) + (4.14 * (MOd - PBd - GD));

		return EMsuino * 10;
	}

	/**
	 * Calcula a energia metabolizavel dos alimentos de origem animal.
	 * 
	 * fórmula EMaves = 4,31(PBd)+9,29(GD)+4,14(ENNd)
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */

	public Double suinoEnergiaMetabolizavelAnimal(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double g = 0.0; // gordura bruta
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		Double EMsuino = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {
			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta " + PB);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				g = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura bruta " + g);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta digestivel " + PBd);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura digestivel " + GD);
			}

		}

		EMsuino = (4.952 * (PB * (PBd / 100))) + (9.45 * (g * (GD / 100)));

		return EMsuino * 10;
	}

	/**
	 * Calcula a energia metabolizavel dos alimentos de origem carboidrato e
	 * gordura.
	 * 
	 * fórmula EMaves = 4,31(PBd)+9,29(GD)+4,14(ENNd)
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */

	public Double suinoEnergiaMetabolizavelCarboidratosGorduras(List<AlimentoNutriente> listaNutrientes) {

		Double ED = 0.0; // Energia Dig. Su�nos, kcal/kg
		Double EMsuino = 0.0; // Fibra bruta

		EMsuino = 0.965 * ED;

		return EMsuino * 10;
	}

	/**
	 * Calcula a energia liquida suino.
	 * 
	 * fórmula EMaves = 4,31(PBd)+9,29(GD)+4,14(ENNd)
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia liquida
	 */

	public Double suinoEnergiaLiquida(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double FB = 0.0; // Fibra bruta
		Double G = 0.0; // gordura bruta
		Double A = 0.0; // Amido
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		Double EMsuino = 0.0; // Energia metabolizavel suino
		Double ELsuino = 0.0; // Energia liquida

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {
			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta " + PB);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				G = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura bruta " + G);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("141")) {
				FB = alimentoNutriente.getValorNutritivo();
				System.out.println("Fibra bruta " + FB);
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta digestivel " + PBd);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura digestivel " + GD);
			}

		}

		ELsuino = (0.73 * EMsuino) + (13.1 * G) + (3.7 * A) - ((6.7 * PB) - (9.7 * FB));
		return ELsuino;
	}

	/**
	 * Calcula a energia disgetivel dos alimentos de origem vegetal para porcas.
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */
	public Double porcaEnergiaDigestivelVegetal(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double g = 0.0; // gordura bruta
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		Double MOd = 0.0; // Mat. Org�nica Dig. Su�nos, g/kg
		Double MOND = 0.0; // Mat. Org�nica N�o Digerida Su�nos, g/kg
		Double EDporca = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {
			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta " + PB);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				g = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura bruta " + g);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta digestivel " + PBd);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura digestivel " + GD);
			}

		}

		EDporca = (5.65 * (PB * (PBd / 100))) + (9.45 * (g * (GD / 100))) + (4.14 * (MOd - PBd - GD)) + MOND;

		return EDporca * 10;
	}

	/**
	 * Calcula a energia disgetivel dos alimentos de origem vegetal para porcas.
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */
	public Double porcaEnergiaMetabolizavelVegetal(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double g = 0.0; // gordura bruta
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		Double MOd = 0.0; // Mat. Org�nica Dig. Su�nos, g/kg
		Double MOND = 0.0; // Mat. Org�nica N�o Digerida Su�nos, g/kg
		Double EMporca = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {
			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta " + PB);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				g = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura bruta " + g);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta digestivel " + PBd);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura digestivel " + GD);
			}

		}

		EMporca = (4.952 * (PB * (PBd / 100))) + (9.45 * (g * (GD / 100))) + (4.14 * (MOd - PBd - GD)) + (0.75 * MOND);

		return EMporca * 10;
	}

	/**
	 * Calcula a energia liquida suino.
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia liquida
	 */

	public Double porcaEnergiaLiquida(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double FB = 0.0; // Fibra bruta
		Double G = 0.0; // gordura bruta
		Double A = 0.0; // Amido
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		Double EMsuino = 0.0; // Energia metabolizavel suino de origem vegetal
		Double ELsuino = 0.0; // Energia liquida
		
		EMsuino = suinoEnergiaMetabolizavelVegetalLacteos(listaNutrientes);
		
		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {
			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta " + PB);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				G = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura bruta " + G);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("141")) {
				FB = alimentoNutriente.getValorNutritivo();
				System.out.println("Fibra bruta " + FB);
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta digestivel " + PBd);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura digestivel " + GD);
			}

		}

		ELsuino = (0.73 * EMsuino) + (13.1 * G) + (3.7 * A) - ((6.7 * PB) - (9.7 * FB));
		return ELsuino;
	}
}