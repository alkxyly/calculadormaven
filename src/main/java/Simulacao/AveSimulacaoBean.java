package Simulacao;


import java.util.List;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Alimento.Alimento;
import Alimento.AlimentoRN;
import AlimentoNutrientes.AlimentoNutriente;
import AlimentoNutrientes.AlimentoNutrienteRN;

@ManagedBean(name = "aveSimulacaoBean")
@SessionScoped
public class AveSimulacaoBean {
	private List<Alimento> listAlimento;
	private Alimento alimentoSelecionado = new Alimento();
	private List<AlimentoNutriente> listAlimentoNutriente;
	private List<AlimentoNutriente> listAlimentoNutrienteSimular;
	private boolean naoEditavel;
	
	private Double avesJovensEnergiaMetabolizada;
	private Double galinhasEnergiaMetabolizada;
	
	public AveSimulacaoBean(){
		this.avesJovensEnergiaMetabolizada = 0.0;
		this.galinhasEnergiaMetabolizada = 0.0;
	}
	
	public List<Alimento> getListAlimento() {
		AlimentoRN alimentoRN = new AlimentoRN();
		
		if(this.listAlimento == null){
			this.listAlimento = alimentoRN.listarTodos();
		}
		return listAlimento;
	}	
	/**
	 * Ao Selecionar o alimento é adicionado os nutrientes as listas que 
	 * irão preencher as tabelas.
	 */
	public void selecionar(){
		AlimentoNutrienteRN al = new AlimentoNutrienteRN();
		AveSimulacaoRN AS = new AveSimulacaoRN();
		this.listAlimentoNutriente = al.listaComAlimento(alimentoSelecionado);
		System.out.println(listAlimentoNutriente.size());
		this.listAlimentoNutrienteSimular = listAlimentoNutriente;
		this.avesJovensEnergiaMetabolizada = Math.ceil(AS.calcularAvesJovensEnergiaMetabolizada(listAlimentoNutriente, alimentoSelecionado.getOrigem()));;
		this.galinhasEnergiaMetabolizada = Math.ceil(AS.calcularGalinhasEnergiaMetabolizada(listAlimentoNutriente, alimentoSelecionado.getOrigem()));

	}

	public void setListAlimento(List<Alimento> listAlimento) {
		this.listAlimento = listAlimento;
	}
	public boolean naoEditavel(Integer codigo){
            this.naoEditavel = false;

		if(codigo.equals(100)){
			this.naoEditavel = true;
		}
		if(codigo.equals(111)){
			this.naoEditavel = true;
		}
		if(codigo.equals(730)){
			this.naoEditavel = true;
		}
		if(codigo.equals(141)){
			this.naoEditavel = true;
		}
		if(codigo.equals(151)){
			this.naoEditavel = true;
		}
		if(codigo.equals(710)){
			this.naoEditavel = true;
		}
		if(codigo.equals(700)){
			this.naoEditavel = true;
		}
		if(codigo.equals(121)){
			this.naoEditavel = true;
		}
		
		
		return !naoEditavel;
	}
	
	

	public boolean isNaoEditavel() {
		return naoEditavel;
	}

	public void setNaoEditavel(boolean naoEditavel) {
		this.naoEditavel = naoEditavel;
	}

	public void setAvesJovensEnergiaMetabolizada(Double avesJovensEnergiaMetabolizada) {
		this.avesJovensEnergiaMetabolizada = avesJovensEnergiaMetabolizada;
	}

	public void setGalinhasEnergiaMetabolizada(Double galinhasEnergiaMetabolizada) {
		this.galinhasEnergiaMetabolizada = galinhasEnergiaMetabolizada;
	}

	public Alimento getAlimentoSelecionado() {
		return alimentoSelecionado;
	}

	public void setAlimentoSelecionado(Alimento alimentoSelecionado) {
		this.alimentoSelecionado = alimentoSelecionado;
	}

	public List<AlimentoNutriente> getListAlimentoNutriente() {
		return listAlimentoNutriente;
	}

	public void setListAlimentoNutriente(List<AlimentoNutriente> listAlimentoNutriente) {
		this.listAlimentoNutriente = listAlimentoNutriente;
	}

	public List<AlimentoNutriente> getListAlimentoNutrienteSimular() {
		return listAlimentoNutrienteSimular;
	}

	public void setListAlimentoNutrienteSimular(List<AlimentoNutriente> listAlimentoNutrienteSimular) {
		this.listAlimentoNutrienteSimular = listAlimentoNutrienteSimular;
	}

	public double getAvesJovensEnergiaMetabolizada() {
		return avesJovensEnergiaMetabolizada;
	}

	public void setAvesJovensEnergiaMetabolizada(double avesJovensEnergiaMetabolizada) {
		this.avesJovensEnergiaMetabolizada = avesJovensEnergiaMetabolizada;
	}

	public double getGalinhasEnergiaMetabolizada() {
		return galinhasEnergiaMetabolizada;
	}

	public void setGalinhasEnergiaMetabolizada(double galinhasEnergiaMetabolizada) {
		this.galinhasEnergiaMetabolizada = galinhasEnergiaMetabolizada;
	}
	
	
	
	
}
