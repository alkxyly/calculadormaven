package Alimento;

import java.util.List;


import FiltrosBusca.AlimentoFilter;
import Util.DAOFactory;

public class AlimentoRN {
	private AlimentoDAO alimentoDAO;

	public AlimentoRN(){
		this.alimentoDAO =  DAOFactory.criarAlimentoDAO();
	}

	/**
	 * @author alkxly
	 * @since 12 de setembro de 2015
	 * @version 1.0
	 * Método responsável por chamar a interface que o objeto
	 * que manipula o hibernate implementa;
	 * 
	 */
	public void salvar(Alimento alimento){
		this.alimentoDAO.salvar(alimento);
	}

	/**
	 * @author alkxly
	 * @param alimento - objeto que será excluido do banco de dados
	 */
	public void excluir(Alimento alimento){
		if(alimento != null){
			this.alimentoDAO.excluir(alimento);	
		}
	}
	/**
	 * @autor Rodrigo
	 * 
	 */
	public void atualizar(Alimento alimento){
		this.alimentoDAO.atualizar(alimento);
	}
	/**
	 * @author alkxly
	 * @return - lista com todos os alimentos cadastrados no banco de dados.
	 */
	public List<Alimento> listar(AlimentoFilter alimentoFilter){
			String codigo = alimentoFilter.getCodigo();
			if(!(codigo.equals(""))){
				return this.alimentoDAO.listarPorCodigo(codigo);
			}else
				return this.alimentoDAO.listar();
	}
	/**
	 * @autor alkxyly
	 * @param codigo - id do alimento a ser buscado no banco de dados.
	 * @return
	 */
	public Alimento carregar(Integer codigo){
		return this.alimentoDAO.carregar(codigo);
	}
	/**
	 * @author alkxly
	 * @param alimento 
	 * @return true - associado false - não associado.
	 * 
	 * Verifica se o alimento está associado à algum nutriente.
	 */
	public boolean isAssociado(Alimento alimento){
		boolean isAssociado = false;
		if(alimento.getAlimentoNutriente().size() > 0){
			isAssociado = true;
		}else 
			isAssociado = false;
		return isAssociado;
	}
	/**
	 * Retorna um alimento buscado de acordo com o código.
	 * @param codigo - busca baseado neste atributo
	 * @return alimento
	 */
	public List<Alimento> listarPorCodigo(String codigo){
		return this.alimentoDAO.listarPorCodigo(codigo);
	}
	
	public List<Alimento> listarTodos(){
		return this.alimentoDAO.listar();
	}
}
