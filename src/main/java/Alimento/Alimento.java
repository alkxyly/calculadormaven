
package Alimento;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import AlimentoNutrientes.AlimentoNutriente;
import Origem.Origem;

/**
 *
 * @author Rodrigo
 */

@Entity
@Table(name = "tb_alimentos")
public class Alimento  implements Serializable {

	private static final long serialVersionUID = 5128605455344401892L;

	@Id
	@GeneratedValue	
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="Origem_id")
	private Origem origem;
	
	@Column(name = "codigo")
	private String codigo;
	
	@Column(name = "nome")
	private String nome;
	
	@OneToMany(mappedBy = "alimento",targetEntity = AlimentoNutriente.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<AlimentoNutriente> alimentoNutriente;
	
	

	public Integer getId() {
		return id;
	}

	public Origem getOrigem() {
		return origem;
	}


	public void setOrigem(Origem origem) {
		this.origem = origem;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

	public List<AlimentoNutriente> getAlimentoNutriente() {
		return alimentoNutriente;
	}

	public void setAlimentoNutriente(List<AlimentoNutriente> alimentoNutriente) {
		this.alimentoNutriente = alimentoNutriente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((origem == null) ? 0 : origem.hashCode());
		result = prime * result + ((alimentoNutriente == null) ? 0 : alimentoNutriente.hashCode());
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alimento other = (Alimento) obj;
		if (origem == null) {
			if (other.origem != null)
				return false;
		} else if (!origem.equals(other.origem))
			return false;
		if (alimentoNutriente == null) {
			if (other.alimentoNutriente != null)
				return false;
		} else if (!alimentoNutriente.equals(other.alimentoNutriente))
			return false;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}


}

