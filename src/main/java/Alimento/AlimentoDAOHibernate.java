package Alimento;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;


public class AlimentoDAOHibernate implements AlimentoDAO {
	private Session session;
	
	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public void salvar(Alimento alimento) {
		this.session.save(alimento);
		
	}

	@Override
	public void excluir(Alimento alimento) {
		this.session.delete(alimento);		
	}

	@Override
	public void atualizar(Alimento alimento) {
		this.session.saveOrUpdate(alimento);
		
	}

	@Override
	public Alimento carregar(Integer codigo) {
		return (Alimento) this.session.get(Alimento.class,codigo);
	}

	@Override
	public List<Alimento> listar() {
		Query queySql = this.session.createSQLQuery("select * from tb_alimentos order by codigo asc").addEntity(Alimento.class);
		List<Alimento> lista = queySql.list();
		return lista;
//		return  (List<Alimento>) this.session.createSQLQuery("select * from tb_alimentos").addEntity(Alimento.class);
			//	createCriteria(Alimento.class).list();
	}

	@Override
	public List<Alimento> listarPorCodigo(String codigo) {
		
		Query hql = this.session.createQuery("from Alimento as alimento where alimento.codigo like :codigo");
		hql.setParameter("codigo",codigo+"%");
		List<Alimento> lista=  hql.list();
		return lista;
	}

}
