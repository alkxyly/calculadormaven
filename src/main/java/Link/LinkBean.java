package Link;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
/**
 * 
 * @author alkxly
 * Bean chamado pela página de template, redirecionando para suas devidas
 * funcionalidades.
 */
@ManagedBean(name="linkBean")
@SessionScoped
public class LinkBean {
	/**
	 * @author alkxly
	 * @since 27 de setembro de 2015
	 * @version 1.0
	 * @return redireciona para página de cadastro de alimento
	 */
	public String cadastrarAlimento(){
		return "/admin/alimentos/CadastrarAlimentos?faces-redirect=true";
	}
	/**
	 * @author alkxly
	 * @since 30 de setembro de 2015
	 * @version 1.0
	 * @return redireciona para a página de cadastro de nutriente
	 */
	public String cadastrarNutriente(){
		return "/admin/nutriente/CadastrarNutriente?faces-redirect=true";
	}
	/**
	 * @author alkxly
	 * @since 30 de setembro de 2015
	 * @version 1.0
	 * @return redireciona para a página de cadastro  de origem
	 */
	public String cadastrarOrigem(){
		return "/admin/Origem/CadastrarOrigem?faces-redirect=true";
	}
	/**
	 * @author alkxly
	 * @since 30 de setembro de 2015
	 * @version 1.0
	 * @return redireciona para página de pesquisa de alimentos
	 */
	public String pesquisarAlimento(){
		return "/admin/alimentos/PesquisarAlimentos?faces-redirect=true";
	}
	/**
	 * @author alkxly
	 * @since 30 de setembro de 2015
	 * @version 1.0
	 * @return redireciona para a página de pesquisa de nutrientes
	 */
	public String pesquisarNutriente(){
		return "/admin/nutriente/PesquisarNutriente?faces-redirect=true";
	}
	/**
	 * @author alkxly
	 * @since 30 de setembro de 2015
	 * @version 1.0
	 * @return redireciona para a página de pesquisa de origens
	 */
	public String pesquisarOrigem(){
		return "/admin/Origem/PesquisarOrigem?faces-redirect=true";
	}
	
	/**
	 * @author Rodrigo
	 * *
	 * @since 13 de outubro de 2015
	 * @version 1.0
	 * @return redireciona para a página de pesquisa de alimentos para similar
	 */
	public String pesquisarAlimentoSimulador(){
		return "/cliente/simulador/PesquisarAlimentos?faces-redirect=true";
	}
}
