package Nutriente;

import java.util.List;

import Alimento.Alimento;


public interface NutrienteDAO {
	public void salvar(Nutriente nutriente);
	public void excluir(Nutriente nutriente);
	public void atualizar(Nutriente nutriente);
	public Nutriente carregar(Integer id);
	public List<Nutriente> listar();
	public List<Nutriente> listarNaoAssociados(Alimento alimento);
	public List<Nutriente> listarPorCodigo(String codigo);
}
