package Nutriente;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import Alimento.Alimento;


public class NutrienteDAOHibernate  implements NutrienteDAO{
	private Session session;
	
	
	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public void salvar(Nutriente nutriente) {
		this.session.save(nutriente);
	}

	@Override
	public void excluir(Nutriente nutriente) {
		this.session.delete(nutriente);
	}

	@Override
	public void atualizar(Nutriente nutriente) {
		this.session.update(nutriente);		
	}

	@Override
	public Nutriente carregar(Integer id) {
		return null;
	}

	@Override
	public List<Nutriente> listar() {
		List<Nutriente> lista = null;
		Query querySql = this.session.createSQLQuery("select * from tb_nutrientes order by codigo").addEntity(Nutriente.class);
		lista =  querySql.list();
		return lista;
		//		return this.session.createCriteria(Nutriente.class).list();
	}
	@Override
	public List<Nutriente> listarNaoAssociados(Alimento alimento){
		List<Nutriente> nutrientesNaoAssociados = null;
		Query querySql = this.session.createSQLQuery("select * from tb_nutrientes as n where n.id NOT IN(select an.nutriente_id from tb_alimento_nutriente as an where an.alimento_id = :idAlimento)").addEntity(Nutriente.class).setParameter("idAlimento",alimento.getId());
		nutrientesNaoAssociados = querySql.list();
		return nutrientesNaoAssociados;
	}

	@Override
	public List<Nutriente> listarPorCodigo(String codigo) {
		Query hql = this.session.createQuery("from Nutriente as nutriente where nutriente.codigo like :codigo");
		hql.setParameter("codigo",codigo+"%");
		List<Nutriente> lista=  hql.list();
		return lista;
	}

	

}
