
package Nutriente;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import AlimentoNutrientes.AlimentoNutriente;

/**
 *
 * @author Rodrigo
 * @author alkxyly
 */

@Entity
@Table(name ="tb_nutrientes")
public class Nutriente implements Serializable{

	private static final long serialVersionUID = 7790723990109723393L;

	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(name = "codigo")
	private String codigo;
	
	@Column(name = "nome")
	private String nome;
	@Transient
	private double valor_nutritivo;
	@Transient
	private int unidade;
	
	@OneToMany(mappedBy = "nutriente",targetEntity = AlimentoNutriente.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<AlimentoNutriente> alimentoNutriente;	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((alimentoNutriente == null) ? 0 : alimentoNutriente
						.hashCode());
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + unidade;
		long temp;
		temp = Double.doubleToLongBits(valor_nutritivo);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nutriente other = (Nutriente) obj;
		if (alimentoNutriente == null) {
			if (other.alimentoNutriente != null)
				return false;
		} else if (!alimentoNutriente.equals(other.alimentoNutriente))
			return false;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (unidade != other.unidade)
			return false;
		if (Double.doubleToLongBits(valor_nutritivo) != Double
				.doubleToLongBits(other.valor_nutritivo))
			return false;
		return true;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getValor_nutritivo() {
		return valor_nutritivo;
	}

	public void setValor_nutritivo(double valor_nutritivo) {
		this.valor_nutritivo = valor_nutritivo;
	}

	public int getUnidade() {
		return unidade;
	}

	public void setUnidade(int unidade) {
		this.unidade = unidade;
	}

	public List<AlimentoNutriente> getAlimentoNutriente() {
		return alimentoNutriente;
	}

	public void setAlimentoNutriente(List<AlimentoNutriente> alimentoNutriente) {
		this.alimentoNutriente = alimentoNutriente;
	}

}
