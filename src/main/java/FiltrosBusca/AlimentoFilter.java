package FiltrosBusca;

import java.io.Serializable;

public class AlimentoFilter implements Serializable{
	private static final long serialVersionUID = -5544645002670814335L;

	private String codigo;
	private String nome;
	
	public AlimentoFilter(){
		this.codigo = "";
	}
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
