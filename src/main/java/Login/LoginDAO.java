package Login;

import java.util.List;

public interface LoginDAO {
	public void salvar(Login alimentoNutriente);
	public void excluir(Login alimentoNutriente);
	public void atualizar(Login alimentoNutriente);
	public Login carregar(Integer id);
	public List<Login> listar();
}
