package Aminoacido;

import java.util.List;

import Util.DAOFactory;
/**
 * 
 * @author alkxly
 * @version 1.0
 */
public class AminoacidoRN {
	AminoacidoDAO aminoacidoDAO;
	
	public AminoacidoRN(){
		this.aminoacidoDAO = DAOFactory.criarAminoacidoDAO();
	}
	/**
	 * @autor alkxyly
	 * @since  20 de setembro de 2015
	 * @version 1.0
	 * @param aminoacido
	 */
	public void salvar(Aminoacido aminoacido){
		this.aminoacidoDAO.salvar(aminoacido);
	}
	/**
	 * @autor alkxyly
	 * @since  20 de setembro de 2015
	 * @version 1.0
	 * @return lista com todos os aminoácidos
	 */
	public List<Aminoacido> listar(){
		return this.aminoacidoDAO.listar();
	}
}
