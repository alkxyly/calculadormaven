package Aminoacido;

import java.util.List;

import org.hibernate.Session;


public class AminoacidoDAOHibernate implements AminoacidoDAO{
	private Session session;
	
	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public void salvar(Aminoacido aminoacido) {
		this.session.save(aminoacido);
		
	}

	@Override
	public void excluir(Aminoacido aminoacido) {
		
	}

	@Override
	public void atualizar(Aminoacido aminoacido) {
		
	}

	@Override
	public Aminoacido carregar(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Aminoacido> listar() {
		// TODO Auto-generated method stub
		return  this.session.createCriteria(Aminoacido.class).list();
	}

}
