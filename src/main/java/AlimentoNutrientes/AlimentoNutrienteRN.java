package AlimentoNutrientes;

import java.util.List;

import Alimento.Alimento;
import Nutriente.Nutriente;
import Util.DAOFactory;

public class AlimentoNutrienteRN {
	
	private AlimentoNutrienteDAO alimentoNutrienteDAO;
	
	public AlimentoNutrienteRN(){
		this.alimentoNutrienteDAO = DAOFactory.criarAlimentoNutriente();
	}
	/**
	 * @author alkxly
	 *
	 * Salva o objeto referente à associação de alimento e nutrientes.
	 * @param alimentoNutriente - objeto referente a associação.
	 */
	public void salvar(AlimentoNutriente alimentoNutriente){
		this.alimentoNutrienteDAO.salvar(alimentoNutriente);
	}
	/**
	 * @author alkxly
	 * @since 23 de setembro de 2015
	 * @version 1.0
	 * 
	 * @param alimentoNutriente - entidade referente a associacao
	 * @param alimento - representa um alimento que será associado aos nutrientes. 
	 * @param nutrientes - lista com todos os nutrientes que o alimento deve conter.
	 */
	public void associarAlimento(AlimentoNutriente alimentoNutriente, Alimento alimento, List<Nutriente> nutrientes){
		if(alimento != null && nutrientes != null ){
			alimentoNutriente =  new AlimentoNutriente();
			alimentoNutriente.setAlimento(alimento);
			for (int i = 0; i < nutrientes.size(); i++) {
				double valorNutritivo = nutrientes.get(i).getValor_nutritivo();
				int unidade = nutrientes.get(i).getUnidade();
				alimentoNutriente.setNutriente(nutrientes.get(i));
				alimentoNutriente.setValorNutritivo(valorNutritivo);
				alimentoNutriente.setUnidade(unidade);
				this.salvar(alimentoNutriente);		
			}
		}
		
	}
	/**
	 * @author alkxly
	 * @since 26 de setembro de 2015
	 * @version 1.0
	 * @param alimento - alimento passado por paramentro para verificar se está
	 * ou não associado à algum nutriente.
	 * 
	 * @return true  - alimento está associado
	 *         false - alimento não está associado
	 */
	public boolean isAlimentoAssociado(Alimento alimento){
		return this.alimentoNutrienteDAO.isAlimentoAssociado(alimento);
	}
	/**
	 * @param alimento
	 * @return Lista contendo a associacao baseada no alimento e na lista de nutrientes 
	 * que irão para a parte de simulacão.
	 */
	public List<AlimentoNutriente> listaComAlimento(Alimento alimento){
		return alimentoNutrienteDAO.listaComAlimento(alimento);
	}
}
