package Origem;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Util.Mensagens;



@ManagedBean(name = "origemBean")
@SessionScoped
public class OrigemBean {
	private final String CADASTRO_ORIGEM_JSF = "/admin/Origem/CadastrarOrigem";
	private final String PESQUISAR_ORIGEM_JSF = "/admin/Origem/PesquisarOrigem";
	private Origem origem;
	private List<Origem> listar;
	private Origem origemSelecionado;

	public OrigemBean(){
		this.origem =  new Origem();
		this.origemSelecionado = null;
		this.listar = new ArrayList<Origem>();
	}
	/**
	 * @author Rodrigo
	 * @since 1 de dezembro de 2015
	 * @version 1.0
	 * 
	 */
	public String salvar(){
		OrigemRN origemRN = new OrigemRN();
		if(origem.getId() != null){
			origemRN.atualizar(origem);
			this.origem = new Origem();
			return PESQUISAR_ORIGEM_JSF;
		}else{
			origemRN.salvar(origem);
			this.origem = new Origem();
			Mensagens.adicionarMensagemConfirmacao("Origem cadastrado com sucesso.");
			return CADASTRO_ORIGEM_JSF;
		}
	

	}

	public String editar(){
		return CADASTRO_ORIGEM_JSF;
	}

	public void atualizar(){
		OrigemRN origemRN = new OrigemRN();
		origemRN.atualizar(origem);
		this.origem = null;
	}

	public void excluir(){
		OrigemRN origemRN = new OrigemRN();
		if(this.origemSelecionado != null){
			origemRN.excluir(origemSelecionado);
			Mensagens.adicionarMensagemConfirmacao("Origem excluido com sucesso.");
		}
	}	
	
	public Origem getOrigem() {
		return origem;
	}
	public void setOrigem(Origem origem) {
		this.origem = origem;
	}
	public List<Origem> getListar() {
		OrigemRN origemRN = new OrigemRN();
		this.listar = origemRN.listar();
		return listar;
	}
	public void setListar(List<Origem> listar) {
		this.listar = listar;
	}
	public Origem getOrigemSelecionado() {
		return origemSelecionado;
	}
	public void setOrigemSelecionado(Origem origemSelecionado) {
		this.origemSelecionado = origemSelecionado;
	}
	
	
}
